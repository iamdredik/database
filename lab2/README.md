"Створення додатку бази даних, орієнтованого на взаємодію з СУБД PostgreSQL"
=====================
Структура таблиць бази даних
-----------------------------------
1. years
---
id | PK | serial | not null | |

year | | integer | not null | unique |

2. countries
---
id | PK | serial | not null | |

country | | text | not null | unique |

3. genres
---
id | PK | serial | not null | |

country | | text | not null | unique |

4. musicians
---
- id	PK	serial	not null	
- name		text	not null	
- birthday		timestamp	null	
UNIQUE -> (country_id, name, birthday)

5. albums
---
- id	PK	serial	not null	
- musician_id	FK		not null	
- title		text	not null	
UNIQUE -> (musician_id, title)

6. songs
---
- id	PK	serial	not null	
- album_id	FK		not null	
- title		text	not null	

7. sg
---
- song_id | FK | | not null |
- genre_id | FK | | not null |
- PK -> (song_id, genre_id)

